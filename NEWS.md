## Release History of the airGRteaching Package



### 0.14.2 Release Notes (2024-07-08)

#### New features

- added  `"CemaNeige"` as a possible value of the `fill` argument in `fill_airGR()` and `BasinData()` functions [(#3)](https://gitlab.irstea.fr/HYCAR-Hydro/baseflow/-/issues/3)


#### Version control and issue tracking

- added a NEWS file in order to track changes [(#5)](https://gitlab.irstea.fr/HYCAR-Hydro/baseflow/-/issues/5)
- added CI to automatically run CRAN checks [(#7)](https://gitlab.irstea.fr/HYCAR-Hydro/baseflow/-/issues/7)
- added unit tests [(#8)](https://gitlab.irstea.fr/HYCAR-Hydro/baseflow/-/issues/8)


#### CRAN-compatibility updates

-  updated version of 'extendr-api' (= 0.7.0) so that compiled code does not call non-API entry points in R [(#11)](https://gitlab.irstea.fr/HYCAR-Hydro/baseflow/-/issues/11)
- fix compilation errors on CRAN Windows Server by adding `-lntdll` to `PKG_LIBS` [(#10)](https://gitlab.irstea.fr/HYCAR-Hydro/baseflow/-/issues/10)
- changed the maintainer email [(#9)](https://gitlab.irstea.fr/HYCAR-Hydro/baseflow/-/issues/9)
- changed conditions in order to test object classes [(#4)](https://gitlab.irstea.fr/HYCAR-Hydro/baseflow/-/issues/4)
- replace the use of `personList()` in the 'CITATION' file [(#2)](https://gitlab.irstea.fr/HYCAR-Hydro/baseflow/-/issues/2)
- the package now depends on R (>= 3.5.0)

____________________________________________________________________________________


### 0.13.2 Release Notes (2021-03-19)

#### Bug fixes

- bug fixed for macOS compatibility

____________________________________________________________________________________


### 0.13.0 Release Notes (2021-03-05)


#### New features

- switch from 'rustr' to 'extendr'


#### User-visible changes

- updated INSTALL for compatibility with CRAN policy and macOS

#### CRAN-compatibility updates

- changed Cargo compilation behaviour

____________________________________________________________________________________


### 0.12.3 Release Notes (2021-01-15)


#### New features

- added ARM macOS support

____________________________________________________________________________________


### 0.12.2 Release Notes (2021-01-05)

#### New features

- added pre-compiled MacOS library support

____________________________________________________________________________________


### 0.12.1 Release Notes (2020-12-11)


#### Bug fixes

- bug fixed at build due to rayon version

____________________________________________________________________________________


### 0.12.0 Release Notes (2020-03-16)

#### New features

- added pre-compiled binary archive
- added Cargo version control for non-Windows platforms in 'Makevars' and 'staticlib.R'


#### User-visible changes

- added 'CITATION' file
- optimised the size of the Rust static library


#### CRAN-compatibility updates

- the package now depends on 'airGR' (>= 1.0.5.22) 

____________________________________________________________________________________

	 
### 0.11.2 Release Notes (2019-11-13)


#### New features

- first CRAN version
